// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: triangle.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Trianglegrpc {

  /// <summary>Holder for reflection information generated from triangle.proto</summary>
  public static partial class TriangleReflection {

    #region Descriptor
    /// <summary>File descriptor for triangle.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static TriangleReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "Cg50cmlhbmdsZS5wcm90bxIMdHJpYW5nbGVncnBjIh0KBVBvaW50EgkKAXgY",
            "ASABKAESCQoBeRgCIAEoASJtCghUcmlhbmdsZRIfCgJwMRgBIAEoCzITLnRy",
            "aWFuZ2xlZ3JwYy5Qb2ludBIfCgJwMhgCIAEoCzITLnRyaWFuZ2xlZ3JwYy5Q",
            "b2ludBIfCgJwMxgDIAEoCzITLnRyaWFuZ2xlZ3JwYy5Qb2ludCI/ChNUcmlh",
            "bmdsZUluZm9SZXF1ZXN0EigKCHRyaWFuZ2xlGAEgASgLMhYudHJpYW5nbGVn",
            "cnBjLlRyaWFuZ2xlIjQKEVRyaWFuZ2xlSW5mb1JlcGx5EhEKCXBlcmltZXRl",
            "chgBIAEoARIMCgRhcmVhGAIgASgBMmcKDFRyaWFuZ2xlSW5mbxJXCg9HZXRU",
            "cmlhbmdsZUluZm8SIS50cmlhbmdsZWdycGMuVHJpYW5nbGVJbmZvUmVxdWVz",
            "dBofLnRyaWFuZ2xlZ3JwYy5UcmlhbmdsZUluZm9SZXBseSIAYgZwcm90bzM="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Trianglegrpc.Point), global::Trianglegrpc.Point.Parser, new[]{ "X", "Y" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Trianglegrpc.Triangle), global::Trianglegrpc.Triangle.Parser, new[]{ "P1", "P2", "P3" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Trianglegrpc.TriangleInfoRequest), global::Trianglegrpc.TriangleInfoRequest.Parser, new[]{ "Triangle" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Trianglegrpc.TriangleInfoReply), global::Trianglegrpc.TriangleInfoReply.Parser, new[]{ "Perimeter", "Area" }, null, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class Point : pb::IMessage<Point> {
    private static readonly pb::MessageParser<Point> _parser = new pb::MessageParser<Point>(() => new Point());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<Point> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Trianglegrpc.TriangleReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Point() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Point(Point other) : this() {
      x_ = other.x_;
      y_ = other.y_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Point Clone() {
      return new Point(this);
    }

    /// <summary>Field number for the "x" field.</summary>
    public const int XFieldNumber = 1;
    private double x_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public double X {
      get { return x_; }
      set {
        x_ = value;
      }
    }

    /// <summary>Field number for the "y" field.</summary>
    public const int YFieldNumber = 2;
    private double y_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public double Y {
      get { return y_; }
      set {
        y_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as Point);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(Point other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!pbc::ProtobufEqualityComparers.BitwiseDoubleEqualityComparer.Equals(X, other.X)) return false;
      if (!pbc::ProtobufEqualityComparers.BitwiseDoubleEqualityComparer.Equals(Y, other.Y)) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (X != 0D) hash ^= pbc::ProtobufEqualityComparers.BitwiseDoubleEqualityComparer.GetHashCode(X);
      if (Y != 0D) hash ^= pbc::ProtobufEqualityComparers.BitwiseDoubleEqualityComparer.GetHashCode(Y);
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (X != 0D) {
        output.WriteRawTag(9);
        output.WriteDouble(X);
      }
      if (Y != 0D) {
        output.WriteRawTag(17);
        output.WriteDouble(Y);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (X != 0D) {
        size += 1 + 8;
      }
      if (Y != 0D) {
        size += 1 + 8;
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(Point other) {
      if (other == null) {
        return;
      }
      if (other.X != 0D) {
        X = other.X;
      }
      if (other.Y != 0D) {
        Y = other.Y;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 9: {
            X = input.ReadDouble();
            break;
          }
          case 17: {
            Y = input.ReadDouble();
            break;
          }
        }
      }
    }

  }

  public sealed partial class Triangle : pb::IMessage<Triangle> {
    private static readonly pb::MessageParser<Triangle> _parser = new pb::MessageParser<Triangle>(() => new Triangle());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<Triangle> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Trianglegrpc.TriangleReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Triangle() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Triangle(Triangle other) : this() {
      p1_ = other.p1_ != null ? other.p1_.Clone() : null;
      p2_ = other.p2_ != null ? other.p2_.Clone() : null;
      p3_ = other.p3_ != null ? other.p3_.Clone() : null;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Triangle Clone() {
      return new Triangle(this);
    }

    /// <summary>Field number for the "p1" field.</summary>
    public const int P1FieldNumber = 1;
    private global::Trianglegrpc.Point p1_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Trianglegrpc.Point P1 {
      get { return p1_; }
      set {
        p1_ = value;
      }
    }

    /// <summary>Field number for the "p2" field.</summary>
    public const int P2FieldNumber = 2;
    private global::Trianglegrpc.Point p2_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Trianglegrpc.Point P2 {
      get { return p2_; }
      set {
        p2_ = value;
      }
    }

    /// <summary>Field number for the "p3" field.</summary>
    public const int P3FieldNumber = 3;
    private global::Trianglegrpc.Point p3_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Trianglegrpc.Point P3 {
      get { return p3_; }
      set {
        p3_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as Triangle);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(Triangle other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(P1, other.P1)) return false;
      if (!object.Equals(P2, other.P2)) return false;
      if (!object.Equals(P3, other.P3)) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (p1_ != null) hash ^= P1.GetHashCode();
      if (p2_ != null) hash ^= P2.GetHashCode();
      if (p3_ != null) hash ^= P3.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (p1_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(P1);
      }
      if (p2_ != null) {
        output.WriteRawTag(18);
        output.WriteMessage(P2);
      }
      if (p3_ != null) {
        output.WriteRawTag(26);
        output.WriteMessage(P3);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (p1_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(P1);
      }
      if (p2_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(P2);
      }
      if (p3_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(P3);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(Triangle other) {
      if (other == null) {
        return;
      }
      if (other.p1_ != null) {
        if (p1_ == null) {
          p1_ = new global::Trianglegrpc.Point();
        }
        P1.MergeFrom(other.P1);
      }
      if (other.p2_ != null) {
        if (p2_ == null) {
          p2_ = new global::Trianglegrpc.Point();
        }
        P2.MergeFrom(other.P2);
      }
      if (other.p3_ != null) {
        if (p3_ == null) {
          p3_ = new global::Trianglegrpc.Point();
        }
        P3.MergeFrom(other.P3);
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 10: {
            if (p1_ == null) {
              p1_ = new global::Trianglegrpc.Point();
            }
            input.ReadMessage(p1_);
            break;
          }
          case 18: {
            if (p2_ == null) {
              p2_ = new global::Trianglegrpc.Point();
            }
            input.ReadMessage(p2_);
            break;
          }
          case 26: {
            if (p3_ == null) {
              p3_ = new global::Trianglegrpc.Point();
            }
            input.ReadMessage(p3_);
            break;
          }
        }
      }
    }

  }

  public sealed partial class TriangleInfoRequest : pb::IMessage<TriangleInfoRequest> {
    private static readonly pb::MessageParser<TriangleInfoRequest> _parser = new pb::MessageParser<TriangleInfoRequest>(() => new TriangleInfoRequest());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<TriangleInfoRequest> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Trianglegrpc.TriangleReflection.Descriptor.MessageTypes[2]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public TriangleInfoRequest() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public TriangleInfoRequest(TriangleInfoRequest other) : this() {
      triangle_ = other.triangle_ != null ? other.triangle_.Clone() : null;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public TriangleInfoRequest Clone() {
      return new TriangleInfoRequest(this);
    }

    /// <summary>Field number for the "triangle" field.</summary>
    public const int TriangleFieldNumber = 1;
    private global::Trianglegrpc.Triangle triangle_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Trianglegrpc.Triangle Triangle {
      get { return triangle_; }
      set {
        triangle_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as TriangleInfoRequest);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(TriangleInfoRequest other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(Triangle, other.Triangle)) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (triangle_ != null) hash ^= Triangle.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (triangle_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(Triangle);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (triangle_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(Triangle);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(TriangleInfoRequest other) {
      if (other == null) {
        return;
      }
      if (other.triangle_ != null) {
        if (triangle_ == null) {
          triangle_ = new global::Trianglegrpc.Triangle();
        }
        Triangle.MergeFrom(other.Triangle);
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 10: {
            if (triangle_ == null) {
              triangle_ = new global::Trianglegrpc.Triangle();
            }
            input.ReadMessage(triangle_);
            break;
          }
        }
      }
    }

  }

  public sealed partial class TriangleInfoReply : pb::IMessage<TriangleInfoReply> {
    private static readonly pb::MessageParser<TriangleInfoReply> _parser = new pb::MessageParser<TriangleInfoReply>(() => new TriangleInfoReply());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<TriangleInfoReply> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Trianglegrpc.TriangleReflection.Descriptor.MessageTypes[3]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public TriangleInfoReply() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public TriangleInfoReply(TriangleInfoReply other) : this() {
      perimeter_ = other.perimeter_;
      area_ = other.area_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public TriangleInfoReply Clone() {
      return new TriangleInfoReply(this);
    }

    /// <summary>Field number for the "perimeter" field.</summary>
    public const int PerimeterFieldNumber = 1;
    private double perimeter_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public double Perimeter {
      get { return perimeter_; }
      set {
        perimeter_ = value;
      }
    }

    /// <summary>Field number for the "area" field.</summary>
    public const int AreaFieldNumber = 2;
    private double area_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public double Area {
      get { return area_; }
      set {
        area_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as TriangleInfoReply);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(TriangleInfoReply other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!pbc::ProtobufEqualityComparers.BitwiseDoubleEqualityComparer.Equals(Perimeter, other.Perimeter)) return false;
      if (!pbc::ProtobufEqualityComparers.BitwiseDoubleEqualityComparer.Equals(Area, other.Area)) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (Perimeter != 0D) hash ^= pbc::ProtobufEqualityComparers.BitwiseDoubleEqualityComparer.GetHashCode(Perimeter);
      if (Area != 0D) hash ^= pbc::ProtobufEqualityComparers.BitwiseDoubleEqualityComparer.GetHashCode(Area);
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (Perimeter != 0D) {
        output.WriteRawTag(9);
        output.WriteDouble(Perimeter);
      }
      if (Area != 0D) {
        output.WriteRawTag(17);
        output.WriteDouble(Area);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (Perimeter != 0D) {
        size += 1 + 8;
      }
      if (Area != 0D) {
        size += 1 + 8;
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(TriangleInfoReply other) {
      if (other == null) {
        return;
      }
      if (other.Perimeter != 0D) {
        Perimeter = other.Perimeter;
      }
      if (other.Area != 0D) {
        Area = other.Area;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 9: {
            Perimeter = input.ReadDouble();
            break;
          }
          case 17: {
            Area = input.ReadDouble();
            break;
          }
        }
      }
    }

  }

  #endregion

}

#endregion Designer generated code
