﻿using System;
using Grpc.Core;
using Trianglegrpc;

namespace TriangleInfoClient
{
    class Program
    {
        public static void Main(string[] args)
        {
            Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);

            var client = new TriangleInfo.TriangleInfoClient(channel);

            var reply = client.GetTriangleInfo(
                new TriangleInfoRequest
                {
                    Triangle = new Triangle
                    {
                        P1 = new Point { X = 0, Y = 0 },
                        P2 = new Point { X = 1, Y = 0 },
                        P3 = new Point { X = 0, Y = 1 }
                    }
                });

            Console.WriteLine($"Perimeter = {reply.Perimeter}\tArea = {reply.Area}");

            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
