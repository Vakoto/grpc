setlocal

set PROTOC=%UserProfile%\.nuget\packages\google.protobuf.tools\3.6.1\tools\windows_x64\protoc.exe
set PLUGIN=%UserProfile%\.nuget\packages\grpc.tools\1.16.0\tools\windows_x64\grpc_csharp_plugin.exe

%PROTOC% -I Protos --csharp_out TriangleInfo Protos/triangle.proto --grpc_out TriangleInfo --plugin=protoc-gen-grpc=%PLUGIN%

endlocal