﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using Trianglegrpc;

namespace TriangleInfoServer
{
    class TriangleInfoImplementation : TriangleInfo.TriangleInfoBase
    {
        private static double GetDistance(Point p1, Point p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        public override Task<TriangleInfoReply> GetTriangleInfo(TriangleInfoRequest request, ServerCallContext context)
        {
            var triangle = request.Triangle;
            var a = GetDistance(triangle.P1, triangle.P2);
            var b = GetDistance(triangle.P1, triangle.P3);
            var c = GetDistance(triangle.P2, triangle.P3);
            var perimeter = a + b + c;
            var p = perimeter / 2;
            var area = Math.Sqrt(p * (p - a) * (p - b) * (p - c));

            return Task.FromResult(
                new TriangleInfoReply
                {
                    Perimeter = perimeter,
                    Area = area
                });
        }
    }

    class Program
    {
        const int Port = 50051;

        public static void Main(string[] args)
        {
            Server server = new Server
            {
                Services = { TriangleInfo.BindService(new TriangleInfoImplementation()) },
                Ports = { new ServerPort("localhost", Port, ServerCredentials.Insecure) }
            };
            server.Start();

            Console.WriteLine("TriangleInfo server listening on port " + Port);
            Console.WriteLine("Press any key to stop the server...");
            Console.ReadKey();

            server.ShutdownAsync().Wait();
        }
    }
}
